<?php


class oot_demo_Hooks {

  public static function init() {
    drupal_set_message('oot_demo_Hooks::init() called');
    $js =  '
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.7.0/build/container/assets/skins/sam/container.css" />
<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/container/container-min.js"></script>
    <script type="text/javascript">
  // Instantiate a Panel from script
  	YAHOO.namespace("ootdemo");

  	function init_ootdemo() {
	YAHOO.ootdemo.panel = new YAHOO.widget.Panel("panel2", { width:"320px", visible:true, draggable:true, close:true } );
	YAHOO.ootdemo.panel.setHeader("OOTools demo");
	YAHOO.ootdemo.panel.setBody("This is a dynamically generated Panel.");
//	YAHOO.ootdemo.panel.setFooter("End of Panel #2");
	YAHOO.ootdemo.panel.render("container");
	}
   YAHOO.util.Event.addListener(window, "load", init_ootdemo);
</script>';

    // Write this javascript into the page header
    drupal_set_html_head($js);
  }

  public static function help($path, $arg) {
    switch ($path) {
      case 'admin/help#oot_demo':
        // Here is some help text for a custom page.
        return t('<h3>OOTools demo is working, as evidenced by this help hook.</h3>');
    }
  }

}